export interface Ma {
    id: number;
    name?: string;
    uuid: string;
    status?: boolean;
}
