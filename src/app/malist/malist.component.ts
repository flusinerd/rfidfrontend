import { Component, OnInit } from '@angular/core';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-malist',
  templateUrl: './malist.component.html',
  styleUrls: ['./malist.component.scss']
})
export class MalistComponent implements OnInit {
  mas = [];
  constructor(private _api:ApiService) { }

  ngOnInit() {
    this._api.getAllMas();
    this._api.maSubject.subscribe(mas => this.mas = mas);
  }

}
