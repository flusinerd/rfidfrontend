import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalistComponent } from './malist.component';

describe('MalistComponent', () => {
  let component: MalistComponent;
  let fixture: ComponentFixture<MalistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
