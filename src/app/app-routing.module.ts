import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MalistComponent } from './malist/malist.component';
import { AdminComponent } from './admin/admin.component';

const routes: Routes = [
  {path: "list", component: MalistComponent},
  {path: "admin", component: AdminComponent},
  { path: '**', redirectTo: 'list', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
