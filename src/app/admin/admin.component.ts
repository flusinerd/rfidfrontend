import { Component, OnInit } from '@angular/core';
import { ApiService } from '../service/api.service';
import { Calendar } from '../interfaces/calendar';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  mas = [];
  selectedMaId = 0;
  selectedMa = {name: ''};
  calendars: Array<Calendar> = [];
  selectedCalendarId = 0;
  selectedCalendar: Calendar = {id: 0, createdAt: new Date(), updatedAt: new Date()};


  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getAllMas();
    this.apiService.maSubject.subscribe(mas => this.mas = mas);
    this.apiService.calendarSubject.subscribe(calendars => this.calendars = calendars);
    this.apiService.getCalendar();
  }

  public changeSelected() {
    this.mas.forEach((ma) => {
      if (ma.id == this.selectedMaId) {
        this.selectedMa = ma;
      }
    });
  }

  public saveChanges() {
    this.apiService.saveMa(this.selectedMa);
  }

  public changeSelectedCalendar() {
    this.calendars.forEach((calendar: Calendar) => {
      if (calendar.id == this.selectedCalendarId) {
        this.selectedCalendar = calendar;
      }
    });
  }

  public saveCalendar() {
    const updatedCalendar = Object.assign({}, this.selectedCalendar);
    updatedCalendar.date = new Date(updatedCalendar.date);
    updatedCalendar.date.setHours(updatedCalendar.date.getHours() - 1);
    updatedCalendar.date.setSeconds(0);
    this.apiService.updateCalendar(updatedCalendar.id, updatedCalendar)
    .then(() => {
      alert('Gespeichert');
    })
    .catch((err) => {
      console.error(err);
    });
  }

  public addCalendar() {
    const calendar = {
      name: this.selectedCalendar.name,
      location: this.selectedCalendar.location,
      date: new Date(this.selectedCalendar.date),
    };
    calendar.date = new Date(calendar.date);
    calendar.date.setHours(calendar.date.getHours() - 1);
    calendar.date.setSeconds(0);
    this.apiService.createCalendar(calendar)
    .then(() => {
      alert('Angelegt');
    })
    .catch((err) => {
      console.error(err);
    });
  }

  public deleteCalendar() {
    this.apiService.deleteCalendar(this.selectedCalendarId)
    .then(() => {
      alert('Gelöscht');
      this.selectedCalendar = {id: 0, createdAt: new Date(), updatedAt: new Date()};
    })
    .catch((err) => {
      console.error(err);
    });
  }

}
