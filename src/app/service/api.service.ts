import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Ma } from '../interfaces/ma';
import { Calendar } from '../interfaces/calendar';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private url = "https://rfid.tensing-gahlen.de/api/";
  public maSubject = new BehaviorSubject([]);
  public calendarSubject = new BehaviorSubject([]);
  constructor(private http:HttpClient) { 
    this.listenToSSE();
   }

  public getAllMas(){
    this.http.get(this.url+"mas").toPromise()
    .then((response:Array<Ma>) =>{
      this.maSubject.next(response);
    })
    .catch((err) =>{
      console.error(err);
    })
  }

  public saveMa(ma){
    let body = new HttpParams({fromObject: ma}).toString();
    let options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }
    this.http.patch(this.url+'mas/'+ma.id, body, options).toPromise()
    .then(() => {
      alert("Saved sucessfull");
    })
  }

  public getCalendar(){
    this.http.get(this.url + 'calendar').toPromise()
    .then((response:Array<Calendar>) => {
      this.calendarSubject.next(response);
    })
    .catch((err) => {
      console.error(err);
    })
  }

  public updateCalendar(id:number, calendar:any):Promise<any>{
    let body = new HttpParams({fromObject: calendar});
    let options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }
    return this.http.patch(this.url + 'calendar/' + id, body, options).toPromise()
  }

  public createCalendar(calendar:any):Promise<any>{
    let body = new HttpParams({fromObject: calendar});
    let options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }
    return this.http.post(this.url + 'calendar', body, options).toPromise()
  }

  public deleteCalendar(id):Promise<any>{
    return this.http.delete(this.url + 'calendar/' + id ).toPromise()
  }

  private listenToSSE(){
    let source = new EventSource("https://rfid.tensing-gahlen.de/stream");

    source.addEventListener('statusChange', () =>{
      this.getAllMas();
    })
    source.addEventListener('created',() =>{
      this.getAllMas();
    })
    source.addEventListener('updatedMa',() =>{
      this.getAllMas();
    })
    source.addEventListener('calendar', () =>{
      this.getCalendar();
    })
  }
}
