import { Component, OnInit } from '@angular/core';
import { ApiService } from '../service/api.service';
import { Calendar } from '../interfaces/calendar';


@Component({
  selector: 'app-calendar-view',
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.scss']
})
export class CalendarViewComponent implements OnInit {

  calendars: Array<Calendar> = [];
  filteredCalendars: Array<Calendar> = [];
  today: Date = new Date();
  constructor(private _api: ApiService) { }

  ngOnInit() {
    this._api.getCalendar();
    this._api.calendarSubject.subscribe(calendars => { this.calendars = calendars; this.filterCalendars(); });
  }

  private filterCalendars() {
    let holdingArray: Array<Calendar> = []
    // console.log(this.calendars);
    this.calendars.forEach((calendar: Calendar) => {
      if (new Date(calendar.date).getDate() == this.today.getDate() && new Date(calendar.date).getMonth() == this.today.getMonth() && new Date(calendar.date).getFullYear() == this.today.getFullYear()) {
        calendar.date = new Date(calendar.date)
        holdingArray.push(calendar);
      }
    })
    holdingArray.sort((a, b) => {
      return new Date(a.date).valueOf() - new Date(b.date).valueOf();
    })
    this.filteredCalendars = holdingArray;

    //Get the current active calendar Entry
    let currentActive;
    let index = -1;
    this.filteredCalendars.forEach((calender: Calendar) => {
      if (calender.date.valueOf() <= Date.now().valueOf()) {
        currentActive = calender;
        index++;
      }
    })
    this.filteredCalendars.splice(0, index);

    //Only show 5 entries
    if (this.filteredCalendars.length > 6){
      this.filteredCalendars.splice(6, this.filteredCalendars.length - 6);
    }

    //Make call in x milliseconds to refresh
    if (this.filteredCalendars[1]) {
      let secondsToNewEntry = this.filteredCalendars[1].date.valueOf() - Date.now().valueOf();
      setTimeout(() => {
        this.filterCalendars();
      }, secondsToNewEntry);
    }
  }

}
